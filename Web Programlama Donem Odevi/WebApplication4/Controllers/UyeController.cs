﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class UyeController : Controller
    {
        public ActionResult Cikis()
        {
            Session["KullanıcıAdı"] = null;
            RedirectToAction("Index", "Home");
            return View();
        }


        public ActionResult Giris()
        {
            DateTime Zaman = DateTime.Now;
            ViewBag.Tarih = Zaman;
            return View();
        }
        [HttpPost]
        public ActionResult Giris(Uye uye)
        {
            var kullanici = db.Uye.FirstOrDefault(x => x.KullanıcıAdı == uye.KullanıcıAdı && x.Sifre == uye.Sifre);
            var Admin = db.Uye.FirstOrDefault(x => x.AdminMi == uye.AdminMi);
            if (kullanici != null)
            {
                Session["KullanıcıAdı"] = kullanici;
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Hata = WebApplication4.Resources.lang.LoginError;
            return View();
        }
        private DbModels db = new DbModels();


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
