﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class AddOrEditController : Controller
    {
        [HttpGet]
        public ActionResult Index(int id=0)
        {
            Uye userModel = new Uye();
            return View(userModel);
        }
        [HttpPost]
        public ActionResult Index(Uye userModel)
        {
            using (DbModels DbModel = new DbModels())
            {
                if(DbModel.Uye.Any( x=>x.KullanıcıAdı == userModel.KullanıcıAdı))
                {
                    ViewBag.AyniKullanici = @WebApplication4.Resources.lang.register1;
                    return View("Index", userModel);
                }

                DbModel.Uye.Add(userModel);
                DbModel.SaveChanges();
            }
            ModelState.Clear();
            ViewBag.KayitMesaj = @WebApplication4.Resources.lang.Kayit;
            return View("Index",new Uye());
        }
    }
}