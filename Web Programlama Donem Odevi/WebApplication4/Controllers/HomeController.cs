﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }
        [Route("Giris")]
        public ActionResult Giris()
        {
            DateTime Zaman = DateTime.Now;
            ViewBag.Tarih = Zaman;
            return View();
        }
        [Route("CalismaSaat")]
        public ActionResult CalismaSaat()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}