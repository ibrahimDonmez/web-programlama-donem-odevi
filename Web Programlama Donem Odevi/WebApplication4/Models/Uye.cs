
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
public partial class Uye
{
    private Uye uye;
    private Uye userModel;

    public Uye()
    {
    }

    public Uye(Uye uye, Uye userModel)
    {
        this.uye = uye;
        this.userModel = userModel;
    }
    public int KullanıcıId { get; set; }
    [Required(ErrorMessage = "Bu Alan Gereklidir.")]

    public string KullanıcıAdı { get; set; }

    [Required(ErrorMessage = "Bu Alan Gereklidir.")]
    [DataType(DataType.Password)]
    public string Sifre { get; set; }

    [DataType(DataType.Password)]
    [DisplayName("Şifre Doğrula")]
    [Compare("Sifre")]
    public string SifreDogrula { get; set; }

    public Nullable<bool> AdminMi { get; set; }
} 